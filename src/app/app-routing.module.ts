import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CarsComponent } from './cars.component';
import { CarDetailComponent } from './car-detail.component';
import { getBackCarComponent } from './getBackCar.component';

const routes: Routes = [
  { path: '', redirectTo: '/cars', pathMatch: 'full' },
  { path: 'detail/:id', component: CarDetailComponent },
  { path: 'cars',     component: CarsComponent },
  { path: 'GetBackCars',     component: getBackCarComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
